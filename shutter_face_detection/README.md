Shutter Face Detection
======================

## Quick Start

### Face Detection with OpenCV

Face detection with the [OpenCV library](https://opencv.org/) can be run through the [opencv_apps](http://wiki.ros.org/opencv_apps) package. A convenient launch file that starts the realsense camera driver and detects faces is provided in this repository:

```bash
$ roslaunch shutter_face_detection detect_faces_opencv.launch [debug_view:=true|[false]] [publish_face_poses:=[true]|false]
```

By default, the faces are then published through the `/shutter_face_detection_opencv/faces` topic. 
If the argument publish_face_poses is true, then the 3D poses for the faces are published through 
`/shutter_face_pose_from_depth/face_poses`.