#!/usr/bin/env python
# Get face detection and compute 3D localization using depth image from RGB-D camera

import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import Image, CameraInfo
from opencv_apps.msg import FaceArrayStamped
from geometry_msgs.msg import PoseArray, Pose
import message_filters
from shutter_perception_utils import image_visualization
import numpy as np

class FacePoseFromDepthNode(object):
    """Node that publishes 3D face locations from 2D detections"""

    def __init__(self):

        rospy.init_node("face_pose_from_depth", anonymous=True)

        # params
        self.opencv_input = rospy.get_param("opencv_input", True)

        # vars
        self.bridge = CvBridge()

        # get camera parameters
        self.color_camera_info = rospy.wait_for_message("/camera/aligned_depth_to_color/camera_info", CameraInfo)

        # publishers
        self.pose_pub = rospy.Publisher("~face_poses", PoseArray, queue_size=5)

        # other subscribers
        if self.opencv_input:
            self.face_sub = message_filters.Subscriber('faces', FaceArrayStamped)
        else:
            rospy.logerr("What kind of input is expected? Check node params.")
            return

        self.depth_sub = message_filters.Subscriber('/camera/aligned_depth_to_color/image_raw', Image)

        self.sync = message_filters.ApproximateTimeSynchronizer([self.face_sub, self.depth_sub], queue_size=5, slop=0.1)
        self.sync.registerCallback(self.faces_callback)

        # do nothing more
        rospy.spin()


    def faces_callback(self, face_msg, depth_msg):
        """
        Callback for the images. Publishes 3D faces
        :param face_msg: detected faces
        :param depth_msg: depth image
        """
        # rospy.loginfo("Got message with stamp {} {}".format(face_msg.header.stamp, depth_msg.header.stamp))

        # camera params
        fx = self.color_camera_info.K[0]
        fy = self.color_camera_info.K[4]
        cx = self.color_camera_info.K[2]
        cy = self.color_camera_info.K[5]
        image_width = self.color_camera_info.width
        image_height = self.color_camera_info.height

        # convert depth Image message to an OpenCV image
        depth_image = self.bridge.imgmsg_to_cv2(depth_msg, desired_encoding="16UC1") # assumes data comes in in mm but as ints

        # prepare output
        pose_array_msg = PoseArray()
        pose_array_msg.header = face_msg.header

        # process each face
        for face in face_msg.faces:

            x = int(face.face.x)
            y = int(face.face.y)
            width = int(face.face.width)
            height = int(face.face.height)

            cropped_depth = depth_image[np.max([y, 0]): np.min([y + height, image_height]),
                                        np.max([x, 0]): np.min([x + width, image_width])]

            indices = np.nonzero(cropped_depth)
            if indices[0].size != 0:

                valid_depth = cropped_depth[indices]

                # image_visualization.visualize_depth_image(cropped_depth)

                # compute average depth value
                avg_depth = np.average(valid_depth.astype(float)) / 1000.0 # convert to meters

                out_x = ((x - cx) * avg_depth) / fx
                out_y = ((y - cy) * avg_depth) / fy
                out_z = avg_depth

            else:

                rospy.loginfo("Empty cropped depth")
                out_x = 0.0
                out_y = 0.0
                out_z = 0.0

            # compute 3D position
            pose_msg = Pose()
            pose_msg.position.x = out_x
            pose_msg.position.y = out_y
            pose_msg.position.z = out_z

            # accumulate
            pose_array_msg.poses.append(pose_msg)

        # publish
        self.pose_pub.publish(pose_array_msg)


if __name__ == '__main__':
    try:
        FacePoseFromDepthNode()
    except rospy.ROSInterruptException:
        pass
