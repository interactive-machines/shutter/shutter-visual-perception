Shutter Visual Perception
=========================

Code for common visual perception tasks in HRI.

## Packages

- **shutter_face_detection:** Face detection
- **shutter_perception_utils:** Utility code for common perception tasks

## Dependencies

- [shutter-ros](https://gitlab.com/interactive-machines/shutter/shutter-ros) v.0.2
- [realsense2_camera](https://github.com/IntelRealSense/realsense-ros) v. 2.2.1 (see the code tag [here](https://github.com/IntelRealSense/realsense-ros/tree/2.2.1))

NOTE: We've tested the realsense2_camera ROS wrapper with LibRealSense v2.21.0. Get access to the LibRealSense versions [here](https://github.com/IntelRealSense/librealsense/releases).

### Installation Instructions

Place this repository within the `src` directory of your catkin workspace, 
along with the shutter-ros and realsense2_camera repositories. 
Then follow the installation instructions for shutter-ros and realsense2_camera, 
and build your workspace.

