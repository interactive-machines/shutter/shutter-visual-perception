# Handy functions for image visualization
import cv2
import numpy as np

def convert_depth_to_heatmap(depth_image):
    """
    Convert depth image to colored heatmap. Helps with visualization!
    :param depth_image: input depth image (format CV_16UC1)
    :return: colored heatmap (format CV_8UC3)
    """
    # compute max value
    max_value = np.max(depth_image)
    # scale depth image to [0,255]
    scaled = depth_image * 255.0 / max_value
    # convert to unsigned ints
    grey_img = np.uint8(scaled)
    # apply color map
    color_img = cv2.applyColorMap(grey_img, cv2.COLORMAP_JET)
    return color_img

def visualize_depth_image(depth_image):
    """
    Visualize depth image using OpenCV
    :param depth_image: depth image (format CV_16UC1)
    """
    # convert from 16UC1 to 8UC3
    color_image = convert_depth_to_heatmap(depth_image)
    # display
    cv2.imshow("depth", color_image)
    cv2.waitKey(1)
