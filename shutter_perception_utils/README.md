Shutter Perception Utils
========================

Package with code for common perception tasks. Right now, it's mainly composed of a
Python module called `shutter_perception_utils`. See the src/shutter_perception_utils for more details.

## Usage

The shutter_perception_utils Python module should be accessible within
your ROS environment once this package is within your catkin workspace.
Within Python, simply import as:

```python
import shutter_perception_utils
```

